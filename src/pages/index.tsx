import type { NextPage } from 'next'
import Head from 'next/head'
import React from 'react';
import { Title, FullDivider, ProductList, CategoryList } from '../components';
import { useFirebase } from '../hooks';
import styles from './Home.module.css'

const Home: NextPage = () => {
  useFirebase();
  return (
    <div className={styles.container}>
      <Head>
        <title>Keshet</title>
        <meta name="description" content="Joyería artesanal hecha con minerales" />
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <main className={styles.main}>
        <ProductList />
        <CategoryList />
      </main>
    </div>
  )
}

export default Home
