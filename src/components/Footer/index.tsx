import styles from "./Footer.module.scss";
import Image from "next/image";

export const Footer = () => { 
    return (
        <footer className={styles.footer}>
            <div className={styles.logo}>
                <Image src="/Logo-gray.svg" alt="Devpand Logo" width={190} height={140} />
            </div>
            <ul className={styles.pagesList}>
                <li><span>Inicio</span></li>
                <li><span>Catálogo</span></li>
                <li><span>Categorías</span></li>
                <li><span>Contacto</span></li>
            </ul>
            <div className={styles.social}>
                <span className={styles.title}>contacto</span>
                <span className={styles.email}>EXCLUSIVIDADESKESHET@GMAIL.COM</span>
                <span className={styles.phone}>tel: 9999-9999</span>
                <div className={styles.strip}>
                    <span className={styles.icon}><Image src="/wa.svg" alt="whatsapp" width={25} height={25}/></span>
                    <span className={styles.icon}><Image src="/ig.svg" alt="whatsapp" width={25} height={25}/></span>
                    <span className={styles.icon}><Image src="/fb.svg" alt="whatsapp" width={25} height={25}/></span>
                </div>
            </div>
        </footer>
    )
}