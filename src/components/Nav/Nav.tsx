import { NavItem, NavItemProps } from "../NavItem";
import Css from "./Nav.module.css";

export interface NavProps {
    items: NavItemProps[]
}

export const Nav = ({ items }: NavProps): JSX.Element => {
    return (
        <div className={Css.Nav}>
            {
                items.map(item => {
                    return <NavItem key={`${item.text}`} {...item} />
                })
            }
        </div>
    )
}