import Css from "./NavItem.module.css";

export interface NavItemProps { 
    text: string;
    uri: string;
    active?: boolean;
}

export const NavItem: React.FC<NavItemProps> = ({text, uri, active}) => { 
    return <a href={uri}><span className={Css.NavItem}>{text}</span></a>
}