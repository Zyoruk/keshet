import Css from "./Product.module.scss";
import Image from "next/image";

export interface CardProps { 
    image: string;
    name: string;
    price: string;
    description: string;
    url: string;
}
export const Card: React.FC<CardProps> = ({description, image, name, price,url}) => { 
    return (
        <div className={Css.ProductContainer}>
            <div className={Css.Product}>
                <Image src={image} alt="" layout="fill" objectFit="fill"/> 
            </div>
        </div>
    )
}