import { Card } from "../Card"
import Css from "./CategoryList.module.css";
import { Title, FullDivider } from "..";

export const CategoryList = () => {
    return (
        <div className={Css.CategoryListContainer}>
            <Title level="h1" text="categorías" externalCss={Css.title}/>
            <FullDivider />
            <div className={Css.CategoryList}>
                {new Array(3).fill(1).map((_, i) => { 
                    return <Card key={`${Date.now().toString()}_${i}`} image="/j_image.png" name="" price="" description="" url=""/>
                }) }
            </div>
        </div>
    );
}