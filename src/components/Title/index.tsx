import { ExternalCss } from "../../interfaces";
import Css from "./Heading.module.css";
import { ClassUtils } from "../../utils";

export interface TitleProps extends ExternalCss { 
    text: string,
    level: "h1" | "h2" | "h3",
    as?: "h1" | "h2" | "h3"
}

export const Title: React.FC<TitleProps> = ({text, level, as, externalCss = ""}) => {
    const getClass = (): string => {
        const _class =  as ? Css[as] : Css[level];
        return ClassUtils.joinClasses(_class, externalCss);
    }

    const getHeader = (): JSX.Element => {
        switch(level){
            case "h1":
                return <h1 className={getClass()} >{text}</h1>;
            case "h2":
                return <h2 className={getClass()}>{text}</h2>;
            case "h3":
                return <h3 className={getClass()}>{text}</h3>
        }
    };

    return getHeader();
}