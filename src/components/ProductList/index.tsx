import { Card } from "../Card"
import Css from "./ProductList.module.css";
import { Title, FullDivider } from "..";

export const ProductList = () => {
    return (
        <>
            <Title level="h1" text="nuevas colecciones" externalCss={Css.title}/>
            <FullDivider />
            <div className={Css.ProductList}>
                {new Array(6).fill(1).map((_, i) => { 
                    return <Card key={`${Date.now().toString()}_${i}`} image="/j_image.png" name="" price="" description="" url=""/>
                }) }
            </div>
        </>
    )
}