import Image from "next/image";
import React from "react";
import Css from "./Header.module.css";
import { Nav } from "../Nav";

export const Header: React.FC = () => { 
    return (
        <div className={Css.Header}>
            <Image src="/Logo.svg" width="190px" height="140px" alt="Keshet Logo depects a pair of kays placed back to back"/>
            <Nav items={[
                {
                    text: "Inicio",
                    uri: "#",
                },
                {
                    text: "Catálogo",
                    uri: "#",
                },
                {
                    text: "Conjuntos",
                    uri: "#",
                },
                {
                    text: "Contacto",
                    uri: "#",
                }
            ]}/>
        </div>
    )
}