import Image from "next/image";
import Css from "./Divider.module.css";

export const FullDivider = () => { 
    return <Image className={Css.divider} src="/Divider.svg" alt="" width={114.55} height={16.36}/>;
}