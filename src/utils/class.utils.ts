const joinClasses = (...classes: string[]): string => { 
    return classes.join(' ');
}

export const ClassUtils = {
    joinClasses
}