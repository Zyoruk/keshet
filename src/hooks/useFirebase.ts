import { initializeApp } from "firebase/app";
import { getAnalytics, isSupported as _isSupported} from "firebase/analytics";
import { useEffect, useState } from "react";

const FIREBASE_CONFIG = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: "keshet-8108c.firebaseapp.com",
    projectId: "keshet-8108c",
    storageBucket: "keshet-8108c.appspot.com",
    messagingSenderId: "413286656475",
    appId: process.env.FIREBASE_APP_ID,
    measurementId: process.env.FIREBASE_MEASURE_ID,
  };

if (typeof global.navigator === 'undefined') global.navigator = {} as Navigator;

export const useFirebase = async () => { 
    const app = initializeApp(FIREBASE_CONFIG);
    const [isSupported, setIsSupported] = useState<boolean>(false);

    useEffect(() => {
        (async () =>{
            setIsSupported(await _isSupported());
        })();
    }, []);
      
    useEffect(() => {
        // Initialize Firebase
        if (isSupported && FIREBASE_CONFIG.apiKey && FIREBASE_CONFIG.appId && FIREBASE_CONFIG.measurementId) { 
          const analytics = getAnalytics(app);
        }
    }, [app, isSupported]);
}